package com.example.examenmovilc1;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;

public class RectanguloActivity extends AppCompatActivity {

    private Rectangulo rectangulo;
    private TextView lblNombre;
    private EditText txtBase, txtAltura;
    private TextView lblArea, lblPerimetro;
    private Button btnCalcular, btnLimpiar, btnRegresar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rectangulo);

        lblNombre = findViewById(R.id.lblNombre);
        txtBase = findViewById(R.id.txtBase);
        txtAltura = findViewById(R.id.txtAltura);
        lblArea = findViewById(R.id.lblArea);
        lblPerimetro = findViewById(R.id.lblPerimetro);
        btnCalcular = findViewById(R.id.btnCalcular);
        btnLimpiar = findViewById(R.id.btnLimpiar);
        btnRegresar = findViewById(R.id.btnRegresar);

        Intent intent = getIntent();
        String nombre = intent.getStringExtra("nombre");
        lblNombre.setText("Mi nombre es " + nombre);

        btnCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    int base = Integer.parseInt(txtBase.getText().toString());
                    int altura = Integer.parseInt(txtAltura.getText().toString());
                    rectangulo = new Rectangulo(base, altura);

                    lblArea.setText("Área: " + rectangulo.calcularArea());
                    lblPerimetro.setText("Perímetro: " + rectangulo.calcularPerimetro());
                } catch (NumberFormatException e) {
                    lblArea.setText("Área: Entrada inválida");
                    lblPerimetro.setText("Perímetro: Entrada inválida");
                }
            }
        });

        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txtBase.setText("");
                txtAltura.setText("");
                lblArea.setText("Área");
                lblPerimetro.setText("Perímetro");
            }
        });

        btnRegresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}